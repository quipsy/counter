import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.WRITE;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.util.Base64;

import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;

@Path("")
public class CounterResource {

    private static final java.nio.file.Path DATA = java.nio.file.Path.of("/data");

    @POST
    @Path("counter/{id: .*}")
    public String increase(@PathParam("id") String id) throws IOException {
        final var file = counterFile(id);
        final String count;
        try (final var fileChannel = FileChannel.open(file, READ, WRITE, CREATE);
                final var reader = new BufferedReader(Channels.newReader(fileChannel, UTF_8));
                final var writer = Channels.newWriter(fileChannel, UTF_8);
                final var lock = fileChannel.lock()) {
            final var line = reader.readLine();
            count = Long.toString(line != null ? Long.parseLong(line) + 1L : 0L);
            fileChannel.truncate(0);
            writer.write(count);
            writer.flush();
        }

        System.out.printf("Increased counter '%s' to %s%n", id, count);

        return count;
    }

    @GET
    @Path("badge/{id: .*}")
    @Produces("image/svg+xml; charset=utf-8")
    public String badge(@PathParam("id") String id, @QueryParam("key_text") @DefaultValue("count") String keyText) throws IOException {
        final var file = counterFile(id);
        final String count;
        if (Files.exists(file))
            try (final var fileChannel = FileChannel.open(file, READ, WRITE);
                    final var reader = new BufferedReader(Channels.newReader(fileChannel, UTF_8));
                    final var lock = fileChannel.lock()) {
                final var line = reader.readLine();
                count = Long.toString(line != null ? Long.parseLong(line) : 0);
            }
        else
            count = "0";

        System.out.printf("Current counter '%s' is %s%n", id, count);

        return """
               <?xml version="1.0" encoding="UTF-8"?>
               <svg xmlns="http://www.w3.org/2000/svg" width="116" height="20">
                 <linearGradient id="b" x2="0" y2="100%%">
                   <stop offset="0" stop-color="#bbb" stop-opacity=".1"/>
                   <stop offset="1" stop-opacity=".1"/>
                 </linearGradient>

                 <mask id="a">
                   <rect width="116" height="20" rx="3" fill="#fff"/>
                 </mask>

                 <g mask="url(#a)">
                   <path fill="#555" d="M0 0 h62 v20 H0 z"/>
                   <path fill="#171fdf" d="M62 0 h54 v20 H62 z"/>
                   <path fill="url(#b)" d="M0 0 h116 v20 H0 z"/>
                 </g>

                 <g fill="#fff" text-anchor="middle">
                   <g font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="11">
                     <text x="31" y="15" fill="#010101" fill-opacity=".3">
                       %2$s
                     </text>
                     <text x="31" y="14">
                       %2$s
                     </text>
                     <text x="89" y="15" fill="#010101" fill-opacity=".3">
                       %1$s
                     </text>
                     <text x="89" y="14">
                       %1$s
                     </text>
                   </g>
                 </g>
               </svg>
               """
               .formatted(count, keyText);
    }

    private static final java.nio.file.Path counterFile(final String id) throws IOException {
        if (Files.notExists(DATA))
            throw new NotFoundException("The folder '/data' does not exist.");

        return DATA.resolve(Base64.getUrlEncoder().withoutPadding().encodeToString(id.getBytes(UTF_8)));
    }

    @GET
    @Path("health")
    public void health() {
        // Implies 204 No Content, i. e. HEALTHY state
    }

}
