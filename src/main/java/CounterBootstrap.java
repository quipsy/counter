import static jakarta.ws.rs.core.Response.Status.Family.REDIRECTION;
import static jakarta.ws.rs.core.Response.Status.Family.SUCCESSFUL;

import java.util.EnumSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import jakarta.ws.rs.SeBootstrap;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.UriBuilder;

public class CounterBootstrap {
    public static void main(final String[] args) throws InterruptedException, ExecutionException {
        final var port = Integer.parseInt(System.getenv().getOrDefault("IP_PORT", "8080"));

        if (args.length > 0 && "--check-health".equals(args[0])) {
            final var ignoreErrors = args.length > 1 && "--ignore-errors".equals(args[1]);
            final var exitCode = checkHealth(port, ignoreErrors).ordinal();
            System.exit(exitCode);
        }

        final var config = SeBootstrap.Configuration.builder().port(port).build();
        SeBootstrap.start(CounterApplication.class, config).thenAccept(instance -> {
            instance.stopOnShutdown(stopResult -> {
                try {
                    System.err.printf("Stop result: %s%n", stopResult);
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            });

            /*
             * AppCDS: Stop immediately once service is up and running
             */
            if (args.length > 0 && "--stop".equals(args[0])) {
                instance.stop().thenRun(() -> System.exit(0));
                System.out.println("Stopping immediately...");
            }

            final var actualPort = instance.configuration().port();
            System.out.printf(
                    "Process %d listening to port %d for POST /counter/{id} and GET /health - Send SIGKILL to shutdown.%n",
                    ProcessHandle.current().pid(), actualPort);
        });

        Thread.currentThread().join();
    }

    private static class CounterApplication extends Application {
        @Override
        public Set<Class<?>> getClasses() {
            return Set.of(CounterResource.class);
        }
    }

    private static enum HEALTH {
        SUCCESS, UNHEALTHY, RESERVED
    }

    private static HEALTH checkHealth(final int port, final boolean ignoreErrors) {
        try {
            final var uri = UriBuilder.newInstance().scheme("http").host("localhost").port(port)
                    .path(CounterResource.class).path(CounterResource.class, "health");
            System.out.println("HEALTHCHECK " + uri);
            if (!EnumSet.of(SUCCESSFUL, REDIRECTION).contains(ClientBuilder.newClient().target(uri).request().get().getStatusInfo().getFamily()))
                throw new WebApplicationException();

            System.out.println("HEALTHY");
            return HEALTH.SUCCESS;
        } catch (final Throwable t) {
            System.out.printf("Unhealthy state detected: %s%n", t.getMessage());
            return ignoreErrors ? HEALTH.SUCCESS : HEALTH.UNHEALTHY;
        }
    }

}
