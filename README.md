# Persistent **Counter** Micro Web Service

## Essential Configuration:
* Bind a docker volume to `/data`.
* Bind external port to `8080`.

## Usage
* `POST /counter/{id}` where `{id}` is a unique name of your counter.
* Returns the current count of that counter.
* Will create the counter on first invocation and initialize it to 0.
* Will increase the counter on subsequent calls.
* `GET /badge/{id}?key_text={name}` where `{id}` is a unique name of your counter, and `{name}` is a text shown on the badge (default is `count`).
* Returns the current count of that counter as an SVG badge.
* Will return 0 if the counter does not exist.

## Administration
* Frequently backup docker volume bound to `/data` to prevent resetting all counters to 0 in desaster case.